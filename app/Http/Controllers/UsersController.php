<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\User; 

class UsersController extends Controller
{
    function index(Request $request)
    {
        //$user = new User();
        //$user->name = "Jainor";
        //$user->email = "Jainor@gmail.com";
        if( $request->isJson() )
        {
            $user= User::all();
            return response()->json($user,200);
        }
        return response()->json(['error'=>'Unauthorized'],401);
    }
    function create(Request $request)
    {
        if( $request->isJson() )
        {
            $datos = $request->json()->all();
            /*
            $user = new User();
            $user->name = $datos['name']; 
            $user->username = $datos['username']; 
            $user->email = $datos['email']; 
            $user->password = Hash::make($datos['password']); 
            $user->api_token = str_random(60); 
            $user->save();
            */
            $user = User::create(
                [
                    'name'=> $datos['name'],
                    'username'=> $datos['username'],
                    'email'=> $datos['email'],
                    'password'=> Hash::make($datos['password']),
                    'api_token'=> str_random(60),
                ]
            );
            return response()->json([$user],200);
        }
        return response()->json(['error'=>'Unauthorized'],401);
    }
    function update(Request $request, $id)
    {
        if( $request->isJson() )
        {
            $datos = $request->json()->all();
            
            $user = User::where('id',$id/*$datos['id']*/)->first();
            $user->name = $datos['name']; 
            $user->username = $datos['username']; 
            $user->email = $datos['email']; 
            $user->api_token = str_random(60); 
            $user->update();
            return response()->json([$user],200);
        }
        return response()->json(['error'=>'Unauthorized'],401);
    }
    function delete(Request $request, $id)
    {
        if( $request->isJson() )
        {
            //$datos = $request->json()->all();
            
            //$user = User::where('id',$datos['id'])->first();
            $user = User::find($id)->first();
            $user->delete();
            return response()->json([$user],200);
        }
        return response()->json(['error'=>'Unauthorized'],401);        
    }
}